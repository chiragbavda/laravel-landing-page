 @extends('includes.master')
 @section('pageTitle','Demo Landing Page')

 @section('content')
<section id="banner" class="banner">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="wow fadeInLeft">Et si le vrai luxe, c'etait I' ecapace?</h1>
                        <div class="banner-text wow fadeInLeft" data-wow-duration="1.5s">
                            <p>Vous souhaitez realiser un voyage de reve, payer des sorti ou offrir plus a vos proches ?</p>
                            <p>Le complement de revenus realise grace a Sharebee permet a nos mois en moyenne et financer toutes sortes de projects.</p>
                        </div>
                        <div class="banner-btn col-md-12 wow fadeInLeft" data-wow-duration="2s">
                            <button class="btn col-md-7 primary-btn"> Proposez votre espace </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Section  -->
    <section id="banner-section" class="spacing banner-section">
        <div class="container">
            <div class="section-title">
                <h2 class="main-title wow bounceIn">
                    Ce que votre espace Sharebee pourrait vous rapporter a <span class="yellow">Montreal </span>
                </h2>
            </div>
            <div class="row">
                <div class="col-md-4 animated fadeInLeft">
                    <div class="form-group">
                        <div class="radio-groups">
                            <label class="radio-label">Stationement
                              <input type="radio" checked="checked" name="radio">
                              <span class="checkmark"></span>
                            </label>
                            <label class="radio-label">Entreposage
                              <input type="radio" name="radio">
                              <span class="checkmark"></span>
                            </label>
                            <label class="radio-label">Les Deux
                              <input type="radio" name="radio">
                              <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 wow fadeInLeft">
                    <div class="input-text">
                        <div class="form-group col-md-12">
                            <input type="text" class="form-control" placeholder="Montreal" />
                        </div>
                        <div class="form-group col-md-11">
                            <select class="form-control">
                                <option>Exteriur</option>
                                <option>Exteriur</option>
                                <option>Exteriur</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 wow fadeInLeft" data-wow-duration="2s">
                    <div class="banner-section-btn col-md-9 col-md-offset-3">
                        <div class="price-btn text-center yellow-bg">
                            <p class="price">986$</p>
                            <p>en movenne par annee</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Section  -->

    <!-- Third Section -->
    <section id="third-section" class="third-section spacing gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <div class="center-img">
                        <img src="img/home.png" />
                    </div>
                    <h2 class="main-title wow bounceIn">Faites travailler votre espace libre a votre place. En un claquement de doigts.</h2>

                    <ol class="wow fadeIn" data-wow-duration="1.5s">
                        <li>Creez votre annonce gratuitement</li>
                        <li>Les locataires decouvrent votre espace</li>
                        <li>Vous accueillez les locataires</li>
                    </ol>
                    <p>Premiers pas</p>
                    <div class="banner-btn col-md-12">
                        <button class="btn gray-btn col-md-2">
                            <i class="fa fa-plus"></i> En savoir plus
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Third Section -->

    <!-- Forth Section Sharebee -->
    <section id="sharebee-section4" class="sharebee-section4 spacing">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="center-img">
                        <img src="img/dish.png" />
                    </div>
                    <div class="section-title">
                        <h2 class="main-title wow bounceIn text-center">Sharebee, Votre concierge personnel</h2>
                    </div>
                    <div class="ques-ans">
                        <ul class="wow fadeIn" data-wow-duration="1.5s">
                            <li>
                                <h4>Vous avez dit fiabilite?</h4>
                                <p>
                                    Oubliez les annonces clasee, ses conversations qui ne menent nulle part et des locataires douteux.
                                </p>
                                <p>
                                    Nous verifions hacun de nos utilisateurs pour que vous n'ayez pas a le faire.
                                </p>
                            </li>
                            <li>
                                <h4>Services aux proprietaires.</h4>
                                <p>
                                    Chacun des versements vous st envoye automatiquement tous les mois. Rien de plus simple. Rien d'autre a faire.
                                </p>
                            </li>
                        </ul>
                    </div>
                    <div class="banner-btn col-md-12 text-center">
                        <button class="btn primary-btn col-md-3"> Proposez votre espace </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Forth Section Sharebee -->
    @stop